# Напишите класс, отвечающий за животное.
# Реализуйте в классе атрибуты : количество лап, издаваемый звук, кличка.
# Реализуйте в классе метод "издать звук".
# Количество лап и звук задается при инициализации и имеет ограничения (ограничения придумайте сами).
# Кличка дается после инициализции. Создайте несколько обьектов класса, напр кошка, собака, птица, и тд.

class Animal():
    name = ""
    sound = ""
    paw_count = 0

    def __init__(self, sound: str, paw_count: int):
        try:
            if paw_count <= 4:
                self.paw_count = int(paw_count)
            if sound.isalpha():
                self.sound = sound
        except:
            self.paw_count = 0
            self.sound = ""

    def giveName(self, name):
        self.name = name

    def makeSound(self):
        print(f"Animal {self.name} with {self.paw_count} paws says {self.sound}")


def main():
    dog = Animal("gav", 4)
    dog.giveName("Pushok")

    raven = Animal("kar", 2)
    raven.giveName("Raver")

    dog.makeSound()
    raven.makeSound()

main()